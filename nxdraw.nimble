# Package

version       = "0.0.1"
author        = "nexustix"
description   = "Drawing library for game prototyping"
license       = "BSD2"
srcDir        = "src"


# Dependencies

requires "nim >= 1.0.0"