{.deadCodeElim: on.}
when defined(windows):
  const
    libnxdraw* = "nxdraw.dll"
elif defined(macosx):
  const
    libnxdraw* = "libnxdraw.dylib"
else:
  const
    libnxdraw* = "libnxdraw.so"

type
    NxdrawEventKind* {.size: sizeof(cint).} = enum
        NXDRAW_EVENT_VOID, NXDRAW_EVENT_UNKNOWN, NXDRAW_EVENT_KEY_DOWN,
        NXDRAW_EVENT_KEY_UP, NXDRAW_EVENT_KEY_HELT, NXDRAW_EVENT_TEXT,
        NXDRAW_EVENT_MOUSE_MOVE, NXDRAW_EVENT_MOUSE_DOWN, NXDRAW_EVENT_MOUSE_UP

{.push callConv:cdecl, header: "nxdraw/api/prefix_graphics.h", dynlib: libnxdraw.}
#{.push callConv:cdecl, dynlib: libnxdraw.}

proc c_nxdraw_width(target: cint): cint 
    {.cdecl, importc: "nxdraw_width".}
proc c_nxdraw_height(target: cint): cint 
    {.cdecl, importc: "nxdraw_height".}

proc c_nxdraw_graphics(winW: cint; winH: cint; texW: cint; texH: cint; resizable: cint): cint 
    {.cdecl, importc: "nxdraw_graphics".}

proc c_nxdraw_set_colour_rgba(colour_id: cint; r: cuchar; g: cuchar; b: cuchar; a: cuchar): cint 
    {.cdecl, importc: "nxdraw_set_colour_rgba", discardable.}
proc c_nxdraw_set_colour_rgb(colour_id: cint; r: cuchar; g: cuchar; b: cuchar): cint 
    {.cdecl, importc: "nxdraw_set_colour_rgb", discardable.}

proc c_nxdraw_pset(target: cint; x: cint; y: cint; colour_id: cint): cint 
    {.cdecl, importc: "nxdraw_pset", discardable.}
proc c_nxdraw_pget(target: cint; x: cint; y: cint): culong 
    {.cdecl, importc: "nxdraw_pget".}

proc c_nxdraw_line(target: cint; x1: cint; y1: cint; x2: cint; y2: cint; colour_id: cint): cint 
    {.cdecl, importc: "nxdraw_line", discardable.}
proc c_nxdraw_hline(target: cint; x1: cint; y1: cint; w: cint; colour_id: cint): cint 
    {.cdecl, importc: "nxdraw_hline", discardable.}
proc c_nxdraw_vline(target: cint; x1: cint; y1: cint; h: cint; colour_id: cint): cint 
    {.cdecl, importc: "nxdraw_vline", discardable.}

proc c_nxdraw_rectangle(target: cint; x: cint; y: cint; w: cint; h: cint; colour_id: cint): cint 
    {.cdecl, importc: "nxdraw_rectangle", discardable.}
proc c_nxdraw_rectangleb(target: cint; x: cint; y: cint; w: cint; h: cint; colour_id: cint): cint 
    {.cdecl, importc: "nxdraw_rectangleb", discardable.}

proc c_nxdraw_circle(target: cint; x: cint; y: cint; r: cint; colour_id: cint): cint 
    {.cdecl, importc: "nxdraw_circle", discardable.}
proc c_nxdraw_circleb(target: cint; x: cint; y: cint; r: cint; colour_id: cint): cint 
    {.cdecl, importc: "nxdraw_circleb", discardable.}

proc c_nxdraw_triangle(target: cint; x1: cint; y1: cint; x2: cint; y2: cint; x3: cint; y3: cint; colour_id: cint): cint 
    {.cdecl, importc: "nxdraw_triangle", discardable.}
proc c_nxdraw_triangleb(target: cint; x1: cint; y1: cint; x2: cint; y2: cint; x3: cint; y3: cint; colour_id: cint): cint 
    {.cdecl, importc: "nxdraw_triangleb", discardable.}

proc c_nxdraw_clear(target: cint; colour_id: cint): cint 
    {.cdecl, importc: "nxdraw_clear", discardable.}

proc c_nxdraw_load_image(path: cstring): cint 
    {.cdecl, importc: "nxdraw_load_image".}
proc c_nxdraw_texture(width: cint; height: cint): cint
    {.cdecl, importc: "nxdraw_texture".}
proc c_nxdraw_blit(target: cint; x: cint; y: cint; source: cint): cint 
    {.cdecl, importc: "nxdraw_blit", discardable.}

proc c_nxdraw_printchar_ascii(target: cint; x: cint; y: cint; c: cuchar; colour_fg: cint; colour_bg: cint): cint 
    {.cdecl, importc: "nxdraw_printchar_ascii", discardable.}
proc c_nxdraw_printstring_ascii(target: cint; x: cint; y: cint; str: cstring; colour_fg: cint; colour_bg: cint): cint 
    {.cdecl, importc: "nxdraw_printstring_ascii", discardable.}

proc c_nxdraw_limit(fps_target: cint): cdouble 
    {.cdecl, importc: "nxdraw_limit".}
proc c_nxdraw_show(): cint 
    {.cdecl, importc: "nxdraw_show", discardable.}
proc c_nxdraw_running(): cint 
    {.cdecl, importc: "nxdraw_running".}

proc c_nxdraw_poll_event(): cint 
    {.cdecl, importc: "nxdraw_poll_event".}
proc c_nxdraw_event_kind(): cint 
    {.cdecl, importc: "nxdraw_event_kind".}
proc c_nxdraw_event_key(): cint 
    {.cdecl, importc: "nxdraw_event_key".}
proc c_nxdraw_event_button(): cint 
    {.cdecl, importc: "nxdraw_event_button".}
proc c_nxdraw_event_mods(): cint 
    {.cdecl, importc: "nxdraw_event_mods".}
proc c_nxdraw_event_codepoint(): cuint 
    {.cdecl, importc: "nxdraw_event_codepoint".}
proc c_nxdraw_event_x(): cdouble 
    {.cdecl, importc: "nxdraw_event_x".}
proc c_nxdraw_event_y(): cdouble 
    {.cdecl, importc: "nxdraw_event_y".}


#[
    wrappers for more elegant consumption
]#
{.pop.}


proc nxdraw_width*(target: int): int {.discardable.} =
    return c_nxdraw_width(cint(target))
proc nxdraw_height*(target: int): int {.discardable.} =
    return c_nxdraw_height(cint(target))

proc nxdraw_graphics*(winW: int; winH: int; texW: int; texH: int; resizable: int): int {.discardable.} =
    return c_nxdraw_graphics(cint(winW), cint(winH), cint(texW), cint(texH), cint(resizable))

proc nxdraw_set_colour_rgba*(colour_id: int; r: char; g: char; b: char; a: char): int {.discardable.} =
    return c_nxdraw_set_colour_rgba(cint(colour_id), r, g, b, a)
proc nxdraw_set_colour_rgb*(colour_id: int; r: char; g: char; b: char): int {.discardable.} =
    return c_nxdraw_set_colour_rgb(cint(colour_id), r, g, b)

proc nxdraw_pset*(target: int; x: int; y: int; colour_id: int): int {.discardable.} =
    return c_nxdraw_pset(cint(target),cint(x),cint(y),cint(colour_id))
proc nxdraw_pget*(target: int; x: int; y: int): uint32 =
    return uint32(c_nxdraw_pget(cint(target), cint(x), cint(y)))

proc nxdraw_line*(target: int; x1: int; y1: int; x2: int; y2: int; colour_id: int): int {.discardable.} =
    return c_nxdraw_line(cint(target),cint(x1),cint(y1),cint(x2),cint(y2), cint(colour_id))
proc nxdraw_hline*(target: int; x1: int; y1: int; w: int; colour_id: int): int {.discardable.} =
    return c_nxdraw_hline(cint(target),cint(x1),cint(y1),cint(w),cint(colour_id))
proc nxdraw_vline*(target: int; x1: int; y1: int; h: int; colour_id: int): int {.discardable.} =
    return c_nxdraw_vline(cint(target),cint(x1),cint(y1),cint(h),cint(colour_id))


proc nxdraw_rectangle*(target: int; x: int; y: int; w: int; h: int; colour_id: int): int {.discardable.} =
    return c_nxdraw_rectangle(cint(target),cint(x),cint(y),cint(w),cint(h),cint(colour_id))
proc nxdraw_rectangleb*(target: int; x: int; y: int; w: int; h: int; colour_id: int): int {.discardable.} =
    return c_nxdraw_rectangleb(cint(target),cint(x),cint(y),cint(w),cint(h),cint(colour_id))

proc nxdraw_circle*(target: int; x: int; y: int; r: int; colour_id: int): int {.discardable.} =
    return c_nxdraw_circle(cint(target),cint(x),cint(y),cint(r),cint(colour_id))
proc nxdraw_circleb*(target: int; x: int; y: int; r: int; colour_id: int): int {.discardable.} =
    return c_nxdraw_circleb(cint(target),cint(x),cint(y),cint(r),cint(colour_id))

proc nxdraw_triangle*(target: int; x1: int; y1: int; x2: int; y2: int; x3: int; y3: int; colour_id: int): int {.discardable.} =
    return c_nxdraw_triangle(cint(target),cint(x1),cint(y1),cint(x2),cint(y2),cint(x3),cint(y3),cint(colour_id))
proc nxdraw_triangleb*(target: int; x1: int; y1: int; x2: int; y2: int; x3: int; y3: int; colour_id: int): int {.discardable.} =
    return c_nxdraw_triangleb(cint(target),cint(x1),cint(y1),cint(x2),cint(y2),cint(x3),cint(y3),cint(colour_id))

proc nxdraw_clear*(target: int; colour_id: int): int {.discardable.} =
    return c_nxdraw_clear(cint(target), cint(colour_id))

proc nxdraw_load_image*(path: cstring): int {.discardable.} =
    return c_nxdraw_load_image(path)
proc nxdraw_texture*(width: int; height: int): int {.discardable.} =
    return c_nxdraw_texture(cint(width), cint(height))
proc nxdraw_blit*(target: int; x: int; y: int; source: int): int {.discardable.} =
    return c_nxdraw_blit(cint(target), cint(x), cint(y), cint(source))

proc nxdraw_printchar_ascii*(target: int; x: int; y: int; c: char; colour_fg: int; colour_bg: int): int {.discardable.} =
    return c_nxdraw_printchar_ascii(cint(target),cint(x),cint(y),c,cint(colour_fg),cint(colour_bg))
proc nxdraw_printstring_ascii*(target: int; x: int; y: int; str: string; colour_fg: int; colour_bg: int): int {.discardable.} =
    return c_nxdraw_printstring_ascii(cint(target),cint(x),cint(y),str,cint(colour_fg),cint(colour_bg))


proc nxdraw_limit*(fps_target: int): float64 {.discardable.} =
    return c_nxdraw_limit(cint(fps_target))
proc nxdraw_show*(): int {.discardable.} =
    return c_nxdraw_show();
proc nxdraw_running*(): int {.discardable.} =
    return c_nxdraw_running()


proc nxdraw_poll_event*(): int {.discardable.} = 
    return c_nxdraw_poll_event()

proc nxdraw_event_kind*(): int =
    return c_nxdraw_event_kind()
proc nxdraw_event_key*(): int =
    return c_nxdraw_event_key()
proc nxdraw_event_button*(): int =
    return c_nxdraw_event_button()
proc nxdraw_event_mods*(): int =
    return c_nxdraw_event_mods()
proc nxdraw_event_codepoint*(): uint32 = 
    return c_nxdraw_event_codepoint()
proc nxdraw_event_x*(): float64 =
    return c_nxdraw_event_x()
proc nxdraw_event_y*(): float64 =
    return c_nxdraw_event_y()

